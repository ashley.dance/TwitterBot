<?php
// Increase execution time
set_time_limit (600);

// Require Twitter
require "twitteroauth/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

//CURL reddit to get top 5 posts within 25 hours
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://www.reddit.com/r/wholesomememes/top/.json?limit=5");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$posts = curl_exec($ch);
curl_close($ch);
$posts = json_decode($posts);

// Set count for filename
$count = 1;

// Loop through top 5 and save them as images
foreach ($posts->data->children as $post) {
	// Image URL from
	$img = $post->data->preview->images[0]->source->url;

	// CURL image to save it
	$ch = curl_init($img);
	$fp = fopen('images/meme-'.$count.'.jpg', 'wb');
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);

	$count ++;
}

//  Post 5 tweets to twitter
$access_token = 'YOUR DETAILS HERE';
$access_token_secret = 'YOUR DETAILS HERE';

$connection = new TwitterOAuth('a5lLtPuhLLtt9W1C9z3y3IgGz', 'kylFPlLwMnBYUpLWJ9pZHJlIgu6U9RvVCBehMHxp4F2dp2spvE', $access_token, $access_token_secret);
$connection->setTimeouts(10, 15);


for ($count = 1; $count <= 5; $count++) {

    $media1 = $connection->upload('media/upload', ['media' => 'images/meme-'.$count.'.jpg']);
	$parameters = ['media_ids' => [$media1->media_id_string]];
	$result = $connection->post('statuses/update', $parameters);

}